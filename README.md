# YouTube Blocklist

A backup of my personal blacklist. Every channel I don't want to see is listed here. 
You can import this list into Iridium or make your own.


Download Iridium for Chrome:
https://chrome.google.com/webstore/detail/iridium-for-youtube/gbjmgndncjkjfcnpfhgidhbgokofegbl

Download Iridium for Firefox:
https://addons.mozilla.org/de/firefox/addon/particle-iridium/
